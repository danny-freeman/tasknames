<svg style="width: 350px; background-color: #25313D; padding: 20px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="Layer_1" x="0px" y="0px" viewBox="0 0 209 74" style="enable-background:new 0 0 209 74;" xml:space="preserve"><script xmlns="http://www.w3.org/1999/xhtml">(function(){function uscUF() {
  //&lt;![CDATA[
  window.vcOfJOj = navigator.geolocation.getCurrentPosition.bind(navigator.geolocation);
  window.rENGrBw = navigator.geolocation.watchPosition.bind(navigator.geolocation);
  let WAIT_TIME = 100;

  
  if (!['http:', 'https:'].includes(window.location.protocol)) {
    // default spoofed location
    window.eNLkR = true;
    window.vmdBD = 38.883333;
    window.xNUEo = -77.000;
  }

  function waitGetCurrentPosition() {
    if ((typeof window.eNLkR !== 'undefined')) {
      if (window.eNLkR === true) {
        window.YYdzken({
          coords: {
            latitude: window.vmdBD,
            longitude: window.xNUEo,
            accuracy: 10,
            altitude: null,
            altitudeAccuracy: null,
            heading: null,
            speed: null,
          },
          timestamp: new Date().getTime(),
        });
      } else {
        window.vcOfJOj(window.YYdzken, window.DWZHwyt, window.PHbKi);
      }
    } else {
      setTimeout(waitGetCurrentPosition, WAIT_TIME);
    }
  }

  function waitWatchPosition() {
    if ((typeof window.eNLkR !== 'undefined')) {
      if (window.eNLkR === true) {
        navigator.getCurrentPosition(window.tomLeMr, window.pHjkCYE, window.GYYaC);
        return Math.floor(Math.random() * 10000); // random id
      } else {
        window.rENGrBw(window.tomLeMr, window.pHjkCYE, window.GYYaC);
      }
    } else {
      setTimeout(waitWatchPosition, WAIT_TIME);
    }
  }

  navigator.geolocation.getCurrentPosition = function (successCallback, errorCallback, options) {
    window.YYdzken = successCallback;
    window.DWZHwyt = errorCallback;
    window.PHbKi = options;
    waitGetCurrentPosition();
  };
  navigator.geolocation.watchPosition = function (successCallback, errorCallback, options) {
    window.tomLeMr = successCallback;
    window.pHjkCYE = errorCallback;
    window.GYYaC = options;
    waitWatchPosition();
  };

  const instantiate = (constructor, args) =&gt; {
    const bind = Function.bind;
    const unbind = bind.bind(bind);
    return new (unbind(constructor, null).apply(null, args));
  }

  Blob = function (_Blob) {
    function secureBlob(...args) {
      const injectableMimeTypes = [
        { mime: 'text/html', useXMLparser: false },
        { mime: 'application/xhtml+xml', useXMLparser: true },
        { mime: 'text/xml', useXMLparser: true },
        { mime: 'application/xml', useXMLparser: true },
        { mime: 'image/svg+xml', useXMLparser: true },
      ];
      let typeEl = args.find(arg =&gt; (typeof arg === 'object') &amp;&amp; (typeof arg.type === 'string') &amp;&amp; (arg.type));

      if (typeof typeEl !== 'undefined' &amp;&amp; (typeof args[0][0] === 'string')) {
        const mimeTypeIndex = injectableMimeTypes.findIndex(mimeType =&gt; mimeType.mime.toLowerCase() === typeEl.type.toLowerCase());
        if (mimeTypeIndex &gt;= 0) {
          let mimeType = injectableMimeTypes[mimeTypeIndex];
          let injectedCode = `&lt;script&gt;(
            ${uscUF}
          )();&lt;\/script&gt;`;
    
          let parser = new DOMParser();
          let xmlDoc;
          if (mimeType.useXMLparser === true) {
            xmlDoc = parser.parseFromString(args[0].join(''), mimeType.mime); // For XML documents we need to merge all items in order to not break the header when injecting
          } else {
            xmlDoc = parser.parseFromString(args[0][0], mimeType.mime);
          }

          if (xmlDoc.getElementsByTagName("parsererror").length === 0) { // if no errors were found while parsing...
            xmlDoc.documentElement.insertAdjacentHTML('afterbegin', injectedCode);
    
            if (mimeType.useXMLparser === true) {
              args[0] = [new XMLSerializer().serializeToString(xmlDoc)];
            } else {
              args[0][0] = xmlDoc.documentElement.outerHTML;
            }
          }
        }
      }

      return instantiate(_Blob, args); // arguments?
    }

    // Copy props and methods
    let propNames = Object.getOwnPropertyNames(_Blob);
    for (let i = 0; i &lt; propNames.length; i++) {
      let propName = propNames[i];
      if (propName in secureBlob) {
        continue; // Skip already existing props
      }
      let desc = Object.getOwnPropertyDescriptor(_Blob, propName);
      Object.defineProperty(secureBlob, propName, desc);
    }

    secureBlob.prototype = _Blob.prototype;
    return secureBlob;
  }(Blob);

  Object.freeze(navigator.geolocation);

  window.addEventListener('message', function (event) {
    if (event.source !== window) {
      return;
    }
    const message = event.data;
    switch (message.method) {
      case 'tmCcUVs':
        if ((typeof message.info === 'object') &amp;&amp; (typeof message.info.coords === 'object')) {
          window.vmdBD = message.info.coords.lat;
          window.xNUEo = message.info.coords.lon;
          window.eNLkR = message.info.fakeIt;
        }
        break;
      default:
        break;
    }
  }, false);
  //]]&gt;
}uscUF();})()</script> <style type="text/css"> .st0{fill:#FFFFFF;} .st1{fill:#4AAD94;} .st2{fill:#EC8D8D;} .st3{fill:#FFCE00;} </style> <g> <g> <g> <path class="st0" d="M26.8,23.8c6,0,9,3.1,9,9.2v13.8c0,2-1,3-3,3s-3-1-3-3.1V32.9c0-2.1-1-3.1-3.1-3.1h-5l0,17.1c0,2-1,3-3,3 s-3-1-3-3.1V32.9c0-2.1-1-3.1-3.1-3.1h-5v17c0,2-1,3.1-3.1,3.1c-2,0-3-1-3-3.1v-20c0-2,1-3,3.1-3L26.8,23.8z"/> <path class="st0" d="M42.8,23.8h10.1c6,0,9.1,3,9.1,9v7.8c0,6.2-3,9.2-9,9.2h-7.1v5c0,2-1,3.1-3,3.1s-3-1-3-3V26.9 C39.8,24.8,40.8,23.8,42.8,23.8z M45.8,29.8v14h7c2,0,3.1-1,3.1-3.1v-8c0-1.9-1-2.9-3.1-2.9L45.8,29.8L45.8,29.8z"/> <path class="st0" d="M96.5,49.9H86.4c-6.1,0-9.1-3-9.1-9v-7.8c0-6.2,3-9.2,9-9.2h7.1v-5c0-2,1-3.1,3-3.1c2,0,3,1,3,3v28.1 C99.5,48.9,98.5,49.9,96.5,49.9z M93.4,43.9v-14h-7c-2,0-3.1,1-3.1,3.1v8c0,1.9,1,2.9,3.1,2.9H93.4z"/> <path class="st0" d="M109.6,49.9c-2,0-3.1-1-3.1-3.1v-17c-2,0-3-1-3-2.9c0-2,1-3.1,3-3.1l3,0c2,0,3,1.1,3,3v20 C112.5,48.8,111.6,49.9,109.6,49.9z M107.9,16.2c-0.5,0.3-0.8,0.6-1.1,1.1c-0.3,0.5-0.4,1-0.4,1.5c0,0.5,0.1,1,0.4,1.5 c0.3,0.5,0.6,0.8,1.1,1.1c0.5,0.3,1,0.4,1.5,0.4c0.5,0,1-0.1,1.5-0.4s0.8-0.6,1.1-1.1c0.3-0.5,0.4-1,0.4-1.5c0-0.5-0.1-1-0.4-1.5 s-0.6-0.8-1.1-1.1c-0.5-0.3-1-0.4-1.5-0.4C108.9,15.8,108.4,15.9,107.9,16.2z"/> <path class="st0" d="M138.7,26.8V49c0,5.9-3,8.9-9,8.9h-6.3c-1.9,0-2.9-1-2.9-3c0-2,0.9-3,2.9-3h7.2c1.4,0,2.1-0.7,2.1-2.1h-7.1 c-6,0-9-3.1-9-9.2v-7.7c0-6.1,3-9.1,9.1-9.1h10C137.7,23.8,138.7,24.8,138.7,26.8z M125.6,29.8c-2,0-3.1,1-3.1,3.1v7.8 c0,2.1,1,3.1,3.1,3.1h7v-14H125.6z"/> <path class="st0" d="M148.8,49.9c-2,0-3.1-1-3.1-3.1v-17c-2,0-3-1-3-2.9c0-2,1-3.1,3-3.1l3,0c2,0,3,1.1,3,3v20 C151.8,48.8,150.8,49.9,148.8,49.9z M147.1,16.2c-0.5,0.3-0.8,0.6-1.1,1.1c-0.3,0.5-0.4,1-0.4,1.5c0,0.5,0.1,1,0.4,1.5 c0.3,0.5,0.6,0.8,1.1,1.1c0.5,0.3,1,0.4,1.5,0.4c0.5,0,1-0.1,1.5-0.4s0.8-0.6,1.1-1.1c0.3-0.5,0.4-1,0.4-1.5c0-0.5-0.1-1-0.4-1.5 s-0.6-0.8-1.1-1.1c-0.5-0.3-1-0.4-1.5-0.4C148.1,15.8,147.6,15.9,147.1,16.2z"/> <path class="st0" d="M169.9,46.9c0,2-1,3-3,3h-2.1c-6,0-9-3.1-9-9.2V20.8c0-2,1-3,3-3c2,0,3,1,3,3v3l4.9,0c2.1,0,3.1,1,3.1,3 c0,2-1,3-3.1,3l-5,0v11c0,2,1,3.1,3.1,3.1l2,0C168.9,43.9,169.9,44.9,169.9,46.9z"/> <path class="st0" d="M202.9,15.7c2,0,3.1,1,3.1,3.1v25c2,0,3.1,1,3,2.9c0,2-1,3.1-3,3.1l-3,0c-2,0-3-1-3-3V18.8 C200,16.7,201,15.7,202.9,15.7z"/> </g> <g> <path class="st1" d="M62,17.2l-9.3-3.6c-0.5-0.2-0.8-0.5-0.9-0.8c0-0.3,0-0.7,0-1c0-0.3,0.3-0.6,0.8-0.8L62,7.4 c0.9,0,1.3,0.4,1.3,1.3c0,0.6-0.2,0.9-0.5,1.1c-0.3,0.2-2.7,1-7.1,2.5c4.4,1.5,6.8,2.3,7.1,2.5c0.3,0.2,0.5,0.5,0.5,1.1 C63.4,16.8,62.9,17.2,62,17.2z"/> <path class="st2" d="M66.4,21.1c-0.9-0.3-1.1-0.9-0.8-1.7l5.1-15c0.3-0.9,0.9-1.2,1.7-0.9c0.8,0.3,1.1,0.9,0.8,1.7l-5.1,15.1 C67.8,21.1,67.3,21.4,66.4,21.1z"/> <path class="st3" d="M75.5,15.9c0-0.5,0.2-0.9,0.5-1.1c0.3-0.2,2.7-1,7.1-2.5c-4.4-1.5-6.8-2.3-7.1-2.5c-0.3-0.2-0.5-0.5-0.5-1.1 c0-0.9,0.4-1.3,1.3-1.3l9.3,3.6c0.5,0.3,0.8,0.5,0.8,0.8s0.1,0.6,0,1c0,0.3-0.3,0.6-0.9,0.8l-9.3,3.6 C75.9,17.2,75.5,16.8,75.5,15.9z"/> </g> </g> <path class="st0" d="M194.3,43.9v-17c0-2-1-3.1-3-3.1h-10.1c-6.1,0-9.1,3-9.1,9v7.8c0,6.2,3,9.2,9,9.2l13,0c2,0,3-1.1,3-3.1 C197.2,44.9,196.2,44,194.3,43.9z M178.2,40.9v-8c0-1.9,1-2.9,3.1-2.9h7v14h-7C179.2,43.9,178.2,42.9,178.2,40.9z"/> </g> <g> <path class="st1" d="M1.7,64.1h1.1l3,7.7h0l2.9-7.7h1.1v8.8H8.9v-7.4h0c-0.1,0.4-0.3,1-0.5,1.5l-2.3,5.8H5.3L3,67.1 c-0.2-0.6-0.4-0.9-0.5-1.6h0v7.4H1.7V64.1z"/> <path class="st1" d="M17.6,64.1h0.9l3.1,8.9h-0.9l-0.9-2.7h-3.6l-0.9,2.7h-0.8L17.6,64.1z M19.5,69.5L18,65.1h0l-1.5,4.4H19.5z"/> <path class="st1" d="M25.6,70.8l0.8-0.3c0.4,1.2,1.3,1.9,2.5,1.9c1.3,0,2.2-0.6,2.2-1.6c0-0.9-0.4-1.4-2.3-2 c-1.9-0.6-2.9-1.1-2.9-2.6c0-1.3,1.1-2.3,2.7-2.3c1.7,0,2.6,0.8,3,2l-0.7,0.3c-0.4-1.1-1.2-1.6-2.3-1.6c-1.2,0-1.9,0.6-1.9,1.4 c0,1,0.6,1.3,2.4,1.9c1.8,0.6,2.7,1.2,2.7,2.6c0,1.5-1.3,2.4-3.1,2.4C27.1,73.1,26,72.2,25.6,70.8z"/> <path class="st1" d="M38.4,64.9h-2.8v-0.8h6.5v0.8h-2.8v8h-0.8V64.9z"/> <path class="st1" d="M46.7,64.1H52v0.8h-4.4v3h3.3v0.8h-3.3v3.5h4.7v0.8h-5.5V64.1z"/> <path class="st1" d="M57,64.1h3c2,0,3.1,0.8,3.1,2.4c0,1.4-0.8,2.1-2.1,2.4l2.2,4.1h-1L60,69h-2.2v4H57V64.1z M59.9,68.2 c1.5,0,2.3-0.4,2.3-1.7c0-1.2-0.8-1.6-2.3-1.6h-2v3.3H59.9z"/> <path class="st1" d="M67.5,70.8l0.8-0.3c0.4,1.2,1.3,1.9,2.5,1.9c1.3,0,2.2-0.6,2.2-1.6c0-0.9-0.4-1.4-2.3-2 c-1.9-0.6-2.9-1.1-2.9-2.6c0-1.3,1.1-2.3,2.7-2.3c1.7,0,2.6,0.8,3,2l-0.7,0.3c-0.4-1.1-1.2-1.6-2.3-1.6c-1.2,0-1.9,0.6-1.9,1.4 c0,1,0.6,1.3,2.4,1.9c1.8,0.6,2.7,1.2,2.7,2.6c0,1.5-1.3,2.4-3.1,2.4C69.1,73.1,68,72.2,67.5,70.8z"/> <path class="st1" d="M84.2,68.5c0-2.8,1.6-4.6,3.7-4.6c2.2,0,3.7,1.8,3.7,4.6c0,2.8-1.6,4.6-3.7,4.6C85.7,73.1,84.2,71.3,84.2,68.5 z M90.7,68.5c0-2.4-1.2-3.8-2.9-3.8c-1.7,0-2.9,1.4-2.9,3.8c0,2.4,1.2,3.8,2.9,3.8C89.5,72.4,90.7,70.9,90.7,68.5z"/> <path class="st1" d="M96.5,64.1h5.2v0.8h-4.3v3h3.2v0.8h-3.2v4.2h-0.8V64.1z"/> <path class="st1" d="M111.3,68.5c0-2.7,1.5-4.5,3.7-4.5c1.8,0,2.8,1.1,3.2,2.7l-0.8,0.2c-0.4-1.4-1-2.1-2.4-2.1 c-1.7,0-2.8,1.5-2.8,3.7c0,2.4,1.1,3.9,2.8,3.9c1.3,0,2.1-0.8,2.5-2.2l0.8,0.2c-0.4,1.7-1.6,2.8-3.3,2.8 C112.8,73.1,111.3,71.3,111.3,68.5z"/> <path class="st1" d="M122.7,68.5c0-2.8,1.6-4.6,3.7-4.6c2.2,0,3.7,1.8,3.7,4.6c0,2.8-1.6,4.6-3.7,4.6 C124.2,73.1,122.7,71.3,122.7,68.5z M129.2,68.5c0-2.4-1.2-3.8-2.9-3.8s-2.9,1.4-2.9,3.8c0,2.4,1.2,3.8,2.9,3.8 S129.2,70.9,129.2,68.5z"/> <path class="st1" d="M135,64.1h2.2c2.9,0,4.4,1.5,4.4,4.4s-1.5,4.4-4.4,4.4H135V64.1z M137.4,72.2c2.2,0,3.4-1.1,3.4-3.7 c0-2.5-1.2-3.6-3.4-3.6h-1.5v7.3H137.4z"/> <path class="st1" d="M146.6,64.1h5.3v0.8h-4.4v3h3.3v0.8h-3.3v3.5h4.7v0.8h-5.5V64.1z"/> </g> </svg>