<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <a href="{{ route('names.index') }}">{{ __('Names') }}</a>
        </h2>
    </x-slot>
    
    <div class="py-12">
        <div class="max-w-3xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg py-4 px-6">
                                <h1 class="text-3xl mb-5">Create a new name</h1>

                                <form class="flex flex-col" action="{{ route('names.store') }}" method="POST">
                                    @csrf
                                    <label class="mb-3" for="name">Name</label>
                                    <input id="name" name="name" type="text" class="bg-gray-200 w-2/4 rounded-sm py-2 px-2 mb-5" value="{{ old('name') }}">
                                    <button class="w-1/4 bg-blue-400 text-white py-2 rounded-sm hover:bg-blue-600" type="submit">Create</button>
                                </form>

                                @if($errors->any())
                                    <div class="rounded-md bg-red-50 p-4 mt-2">
                                        <div class="flex">
                                            <div class="flex-shrink-0">
                                                <!-- Heroicon name: x-circle -->
                                                <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                                                </svg>
                                            </div>
                                            <div class="ml-3">
                                                <h3 class="text-sm font-medium text-red-800">
                                                    There were {{ $errors->count() }} @if($errors->count() > 1) errors @else error @endif with your submission
                                                </h3>
                                                <div class="mt-2 text-sm text-red-700">
                                                    <ul class="list-disc pl-5 space-y-1">
                                                        @foreach($errors->all() as $error)
                                                        <li>
                                                            <p>{{ $error }}</p>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</x-app-layout>
