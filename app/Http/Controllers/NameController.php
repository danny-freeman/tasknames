<?php

namespace App\Http\Controllers;

use App\Models\Name;
use Illuminate\Http\Request;

class NameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Fetch all names
        $names = Name::all();

        return view('names.index', compact('names'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('names.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate and create
        Name::create($this->validateData());

        return redirect()->route('names.index')->with('success', 'New name created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Name  $name
     * @return \Illuminate\Http\Response
     */
    public function show(Name $name)
    {
        return view('names.show', compact('name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Name  $name
     * @return \Illuminate\Http\Response
     */
    public function edit(Name $name)
    {
        return view('names.edit', compact('name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Name  $name
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Name $name)
    {
        // Validate data
        $validated = $this->validateData();

        // Update Model
        $name->name = $validated['name'];
        $name->save();

        return back()->with('success', 'Name updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Name  $name
     * @return \Illuminate\Http\Response
     */
    public function destroy(Name $name)
    {
        $name->delete();

        return redirect()->route('names.index')->with('success', 'Name deleted successfully');
    }

    /**
     * Validate the data
     *
     * @param  \App\Models\Name  $name
     * @return \Illuminate\Http\Response
     */
    public function validateData()
    {
        // Validate data
        $validated = request()->validate([
            'name' => 'required|string|max:200|unique:names',
        ]);

        return $validated;
    }

    /**
     * Generate models
     *
     * @param  \App\Models\Name  $name
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request)
    {
        // Create new names
        $generateCount = request()->input('generate_count');

        Name::factory()->count($generateCount)->create();

        return back()->with('success', 'Succesfully generated ' . $generateCount . ' new names.');
    }
}
