<p align="center"><a href="https://mp.digital/" target="_blank"><img src="https://mp.digital/wp-content/uploads/2019/09/logo-1.svg" width="400"></a></p>


## About MP Digital
We are a web development agency in Nottingham. We produce customer engagement solutions, that focuses on your customer’s online purchasing experience.

---
## About Danny Freeman
A freelance web developer usually found with a cup of tea working from a little cottage in Nottingham. I am passionate about developing slick websites that offer the user an awesome experience. I pride myself in the reliability I offer clients, developing reusable code so as a business grows the website can as well. Out of work you’ll catch me sinking putts and/or beers, or working on the cottage.

---
## Summary
This task has been set by Michael at MP Digital and it is to demonstrate my competencies.

Here is the task list:
- [x] New Laravel application
- [x] Authentication
- [x] No public pages except login and register
- [x] Simple ICRUD for unique names
- [x] Simple command to add some names

Key points;
1. Doesn't need to look sexy or anything, Bootstrap (or whatever) is fine.
2. Vue isn't required but feel free to add if you want.

---
## References
- **[Laravel](https://laravel.com/)**
- **[Laravel Jetstream](https://jetstream.laravel.com/)**
- **[Laravel Livewire](https://laravel-livewire.com/)**
- **[Tailwind CSS](https://tailwindcss.com/)**